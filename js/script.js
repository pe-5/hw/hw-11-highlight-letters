let btns = document.querySelectorAll('.btn')

document.addEventListener('keydown', function(event) {
  for (let key of btns){
    key.classList.remove('active')
    if (event.key.toLowerCase() == key.innerHTML.toLowerCase())
      key.classList.add('active')
  }
})
